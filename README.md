# Alice's website repo

[![Netlify Status](https://api.netlify.com/api/v1/badges/97a5fd6f-a2c1-4495-bf6c-50cd7435c45c/deploy-status)](https://app.netlify.com/sites/figments-alice/deploys)

website at: https://figments.art

😉

figments is conceived by Alice-Anne Psaltis with design by Arturo Guzmán Pérez

contributions by Amaal Said | Gina DeCagna | Hannah Wroe | Isabella Baker | Malachy Harvey | Monika Tobel | Teal Griffin

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
