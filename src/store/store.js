import { ref } from "vue"
import { globalVars } from "../vars"
import _ from "lodash"
const url = globalVars.api_url
const worksData = ref(null)
const activeWorkData = ref(null)
const loaded = ref(null)
let error = {
  status: false,
  message: "",
}

const getImages = async (id, tag) => {
  let result = []
  for (const item of id) {
    const res = await fetch(
      `${url}/items/figments_works_files/${item}?fields=directus_files_id.filename_disk,directus_files_id.tags`, {
      method: 'GET',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
      }
    }
    )
    const { data } = await res.json()
    result = [...result, data.directus_files_id]
  }
  const filterResult = result.filter((item) => item.tags[0] === tag)
  return filterResult[0].filename_disk
}

const getAudios = async (id, tag) => {
  let result = []
  for (const item of id) {
    const res = await fetch(
      `${url}/items/figments_works_files_1/${item}?fields=directus_files_id.filename_disk,directus_files_id.tags`, {
      method: 'GET',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
      }
    }
    )
    const { data } = await res.json()
    result = [...result, data.directus_files_id]
  }
  const filterResult = result.filter((item) => item.tags[0] === tag)
  return filterResult[0].filename_disk
}

export default function loadData() {
  const load = async (origin, name) => {
    try {
      if (!origin) {
        return "please add an origin"
      }
      if (origin === "home") {
        if (worksData.value === null) {
          const res = await fetch(`${url}/items/figments_works`, {
            method: 'GET',
            mode: 'cors',
            headers: {
              'Content-Type': 'application/json',
            },
          })
          const data = await res.json()
          const filtered = data.data.filter((work) => work.artist === true)
          for (const work of filtered) {
            work["artwork_id"] = await getImages(work.artwork, "square")
          }
          const shuffled = _.shuffle(filtered)
          setTimeout(() => {
            loaded.value = true
          }, 500)
          worksData.value = shuffled
        }
      } else if (origin === "works") {
        if (activeWorkData.value === null) {
          if (worksData.value !== null) {
            const filterData = worksData.value.filter(
              (work) => work.name === name
            )
            const filtered = filterData[0]
            filtered["audio_id"] = await getAudios(filtered.audios, "main")
            filtered["artwork_id"] = await getImages(filtered.artwork, "main")
            activeWorkData.value = filtered
          } else {
            const response = await fetch(
              `${url}/items/figments_works?filter[name][_eq]=${name}`, {
              method: 'GET',
              mode: 'cors',
              headers: {
                'Content-Type': 'application/json',
              }
            }
            )
            const data = await response.json()
            const activeWork = data.data[0]
            activeWork["audio_id"] = await getAudios(activeWork.audios, "main")
            activeWork["artwork_id"] = await getImages(
              activeWork.artwork,
              "main"
            )
            activeWorkData.value = activeWork
          }
        } else if (
          activeWorkData.value !== null &&
          activeWorkData.value.name !== name
        ) {
          const filterData = worksData.value.filter(
            (work) => work.name === name
          )
          const filtered = filterData[0]
          filtered["audio_id"] = await getAudios(filtered.audios, "main")
          filtered["artwork_id"] = await getImages(filtered.artwork, "main")
          activeWorkData.value = filtered
        }
      }
    } catch (err) {
      error.status = true
      error.message = err
      console.log(err)
    }
  }
  return { worksData, activeWorkData, loaded, error, load }
}
