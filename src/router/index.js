import { createRouter, createWebHistory } from "vue-router"
import Home from "../views/Home.vue"
import About from "../views/About.vue"
import Works from "../views/Works.vue"
// import NotFound from '../views/NotFound.vue'

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/:name/:title",
    name: "Works",
    component: Works,
    props: true,
  },
  // redirect
  {
    path: "/:catchAll(.*)",
    name: "Home",
    component: Home,
  },
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
})

export default router
