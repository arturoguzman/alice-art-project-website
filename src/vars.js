export const globalVars = {
  api_url: "https://api.artgp.xyz",
  cdn_url: "https://figments.arturoguzmanperez.com/directus/",
}
/* 
if (
  process.env.NODE_ENV === "production" ||
  process.env.NODE_ENV === "preview"
) {
  // For production/preview & netlify
  globalVars.api_url = process.env.API_URL
  globalVars.cdn_url = process.env.CDN_URL
} else {
  // For development
  globalVars.api_url = import.meta.env.VITE_API_URL
  globalVars.cdn_url = import.meta.env.VITE_CDN_URL
}
 */
